/**
 * Created by pohchye on 4/7/2016.
 */

// Create the Express app
var express=require("express");
var app=express();

// Require this for POST method
var bodyParser=require("body-parser");
app.use(bodyParser.urlencoded({'extended': 'true'}));
app.use(bodyParser.json());

// Contain the tasks on the server
var serverToDoList = [];

// Display Task Function
var display_tasks = function () {
    console.info("Server Task List: ");
    var arr_obj = {};
    var prop = "";
    for (var array_index in serverToDoList) {
        if (serverToDoList.hasOwnProperty(array_index)) {
            console.info(">> Index: %s", array_index);
            arr_obj = serverToDoList[array_index];
            for (prop in arr_obj) {
                if (arr_obj.hasOwnProperty(prop)) {
                    console.info("> " + prop + " = " + arr_obj[prop]);
                }
            }
        }
    }
};

// Array to contain tasks in server
serverToDoList = [];

// Add single task from client
app.post("/add", function (req, res) {
    // Retrieve task
    var task = JSON.parse(req.body.params.task);
    console.info("\n>>> Task added to server: " + task.task);
    console.info("Type for task: %s", typeof task);
    // Display individual task
    // for (var key in task) {
    //     if (task.hasOwnProperty(key)) {
    //         var value = task[key];
    //         console.info("> Key: %s || Value: %s", key, value);
    //     }
    // }
    // Add to server task list
    serverToDoList.push(task);
    // Print out server tasks list
    display_tasks();
    // Send status back to client
    res.status(202).end();
});

// Update task that are done


// Delete task



// Save all tasks from client
app.post("/save", function (req, res) {
    // Get the list of tasks
    serverToDoList = JSON.parse(req.body.params.save_list);
    // serverToDoList = req.body.params.save_list;
    console.info("\nserverToDoList: " + serverToDoList);
    // Print out the tasks pass from client
    console.info("Type for serverToDoList: %s", typeof serverToDoList);
    console.info("Save to server: " + serverToDoList.length + " records");
    // Print out server tasks list
    display_tasks();
    res.status(202).end();
});

// Serve public files
app.use(express.static(__dirname + "/public"));
// Handle Errors
app.use(function (req, res, http) {
    res.redirect("error.html");
});

// Set port
app.set("port",
    process.argv[2] || process.env.APP_PORT || 3000
);

// Start the Express server
app.listen(app.get("port"), function () {
    console.info("Express server started on port %s", app.get("port"));
});