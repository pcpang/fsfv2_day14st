/**
 * Created by pohchye on 5/7/2016.
 */

var ToDoApp = angular.module("ToDoApp", []);

// Display Task Function
var display_tasks = function ($task_list) {
    console.info("Client Task List: ");
    console.info("Number of tasks: %s", $task_list.length);
    var arr_obj = {};
    var prop = "";
    for (var array_index in $task_list) {
        if ($task_list.hasOwnProperty(array_index)) {
            console.info(">> Index: %s", array_index);
            arr_obj = $task_list[array_index];
            for (prop in arr_obj) {
                if (arr_obj.hasOwnProperty(prop)) {
                    console.info("> " + prop + " = " + arr_obj[prop]);
                }
            }
        }
    }
};

var update_category = function ($category_list, $category, $action) {
    // var index = $category_list.indexOf($category);
    // // If category does not exists, insert the category into category list
    // if (index == -1) {
    //     // $category_list.push({Category: $category, Counter: count});
    //     $category_list.push($category);
    // }
    // console.info("Category List: ");
    // for (var x in $category_list) {
    //     if ($category_list.hasOwnProperty(x)) {
    //         console.info("> " + $category_list[x]);
    //     }
    // }
    var cat_obj = "";
    var found = 0;
    for (var array_index in $category_list) {
        if ($category_list.hasOwnProperty(array_index)) {
            cat_obj = $category_list[array_index];
            if (cat_obj["Category"] == $category) {
                console.info("Category already exists: %s", cat_obj["Category"]);
                console.info("Category count: %d", cat_obj["Counter"]);
                found = 1;
                if ($action == "add") {
                    cat_obj["Counter"] = cat_obj["Counter"]+1;
                } else {
                    if ($action == "delete") {
                        cat_obj["Counter"] = cat_obj["Counter"]-1;
                        if (cat_obj["Counter"] == 0) {
                            $category_list.splice(array_index,1);
                        }
                    }
                }
            }
        }
    }
    if ((found == 0) && ($action == "add")) {
        $category_list.push({Category: $category, Counter: 1});
    }
    console.info("Category List: ");
    for (var index in $category_list) {
        if ($category_list.hasOwnProperty(index)) {
            console.info(">> Index: %s", index);
            cat_obj = $category_list[index];
            for (var cat_prop in cat_obj) {
                if (cat_obj.hasOwnProperty(cat_prop)) {
                    console.info("> " + cat_prop + " = " + cat_obj[cat_prop]);
                }
            }
        }
    }
};

(function () {
    var ToDoCtrl;
    ToDoCtrl = function ($http, $window) {
        var ctrl = this;
        ctrl.length = "";
        ctrl.clientToDoList = [];
        ctrl.category = [{Category: "All", Counter: 0}];
        ctrl.criteria = "";
        ctrl.newTodo = {
            name: "",
            id: "",
            done: "",
            task: "",
            due: "",
            priority: "",
            category: ""
        };
        // Add task function
        ctrl.add = function () {
            console.info("-- Click on Add button --");
            console.info("\n>>> Task added to client: " + ctrl.newTodo.task);
            console.info("Type for task: %s", typeof ctrl.newTodo);
            console.info("Task: %s", ctrl.newTodo.task);
            console.info("Due: %s", ctrl.newTodo.due);
            console.info("Priority: %s", ctrl.newTodo.priority);
            console.info("Category: %s", ctrl.newTodo.category);
            // Add newToDo task to clientToDoList
            ctrl.newTodo.name = "Test1";
            ctrl.newTodo.id = "ID1";
            ctrl.newTodo.done = "false";
            ctrl.clientToDoList.push({name: ctrl.newTodo.name, id: ctrl.newTodo.id, done: ctrl.newTodo.done,
                task: ctrl.newTodo.task, due: ctrl.newTodo.due, priority: ctrl.newTodo.priority, category: ctrl.newTodo.category});
            // Print out list of tasks
            display_tasks(ctrl.clientToDoList);
            // Convert Javascript object to JSON for new task
            console.log("New Task JSON Format: %s", JSON.stringify(ctrl.newTodo));
            // Send newToDo task to server in JSON format
            $http.post("/add", {
                params: {
                    task: JSON.stringify(ctrl.newTodo),
                    headers: {'Content-Type': 'application/json'}
                }
            }).then(function () {
                console.info("Success in adding new task");
                update_category(ctrl.category,ctrl.newTodo.category,"add");
                ctrl.newTodo = {
                    name: "",
                    id: "",
                    done: "",
                    task: "",
                    due: "",
                    priority: "",
                    category: ""
                };
            }).catch(function () {
                console.info("Failure in adding new task");
            });
            console.info("-- End Add button --");
        };

        // Remove completed tasks function
        // Nothing done on server end
        ctrl.remove = function () {
            console.info("-- Click on Remove button --");
            console.info("Remove Completed Tasks: ");
            for (var x = 0; x < ctrl.clientToDoList.length; x++) {
                console.info("> " + ctrl.clientToDoList[x].task + " " + ctrl.clientToDoList[x].done);
                if (ctrl.clientToDoList[x].done == true) {
                    console.info("Task: " + ctrl.clientToDoList[x].task);
                    // Update Category list
                    update_category(ctrl.category,ctrl.clientToDoList[x].category,"delete");
                    // Update done property on server - not done
                    // Remove completed task from client task list
                    ctrl.clientToDoList.splice(x, 1);
                    // Need to reduce x by 1 because 1 row has been remove
                    x--;
                }
            }
            console.info("-- End Remove button --");
        };

        // Delete one task function
        // Nothing done on server end
        ctrl.delete_task = function ($t) {
            console.info("-- Click on Remove Task button --");
            console.info(">>> Task to delete: " + $t.task);
            var index = ctrl.clientToDoList.indexOf($t);
            // Update Category list
            update_category(ctrl.category,$t.category,"delete");
            // Delete selected task from server task list - not done
            // Delete selected task from client task list
            ctrl.clientToDoList.splice(index, 1);
            console.info("Removing 1 Task from client");
            console.info("Number of tasks left: %s", ctrl.clientToDoList.length);
            display_tasks(ctrl.clientToDoList);
            console.info("-- End Remove Task button --");
        };

        // Save all tasks to server
        ctrl.save = function () {
            console.info("-- Click on Save button --");
            $http.post("/save", {
                params: {
                    save_list: JSON.stringify(ctrl.clientToDoList),
                    // save_list: ctrl.clientToDoList
                    headers: {'Content-Type': 'application/json'}
                }
            }).then(function () {
                console.info("Success in save to server");
            }).catch(function () {
                console.info("Failure in save to server");
            });
            console.info("-- End Save button --");
        };

        // To set the ctrl.criteria for the filter criteria to apply on the displaying of tasks
        ctrl.filter_cat = function ($criteria) {
            ctrl.criteria = $criteria;
            if ($criteria == "All") {
                ctrl.criteria = "";
            }
        }
    };

    ToDoApp.controller("ToDoCtrl", ["$http", "$window", ToDoCtrl]);
})();
